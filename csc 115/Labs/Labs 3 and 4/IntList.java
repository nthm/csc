public class IntList {
	private Node head, node;
	private int count;
	
	public IntList() {
		head = node = null;
		count = 0;
	}
	
	public void add(int item, int index) {
		// Shamelessly (mostly) copy-pasting code from .get()
		if (index < 0 || index > count)
			throw new RuntimeException("Index must be [0," + count + ").");
		
		Node tmp = new Node(item);
		if (index == 0) {
			tmp.next = head; // Order is important
			head = tmp;
		} else {
			node = getNode(index - 1);
			tmp.next = node.next;
			node.next = tmp;
		}
		
		count++;
	}
	
	public void remove(int index) {
		if (index < 0 || index >= count)
			throw new RuntimeException("Index must be [0," + count + ").");
		
		if (index == 0) {
			head = head.next;
		} else {
			node = getNode(index - 1);
			node.next = node.next.next;
		}
	}
	
	public String toString() {
		String str = "{";
		for (Node cursor = head; cursor != null; cursor = cursor.next)
			str += cursor.data + (cursor.next != null ? ", " : "");
		
		return str + "}";
	}
	
	public int size() {
		return count;
	}
	
	public boolean isEmpty() {
		return count == 0;
	}
	
	public Node getNode(int index) {
		if (index < 0 || index >= count)
			throw new RuntimeException("Index must be [0," + count + ").");
		
		node = head;
		int currentIndex = 0;
		while (currentIndex != index) {
			node = node.next;
			currentIndex++;
		}
		
		return node;
	}
	
	public int get(int index) {
		return getNode(index).data;
	}
	
	public static void main(String[] args) {
	/*
		We used the following commented out code to explain how memory is 
		handled at a lower level.
	
		int i = 3;
		int[] intArray = new int[3];
		intArray[2] = i;
		
		Time t1 = new Time(3, 2);
		int[] timeArray = new Time[3];
		timeArray[1] = t1;
	*/
		IntList list = new IntList();
		
		System.out.println("List when empty: " + list);
		
		for (int i = 0; i < 5; i++) {
			list.add(i, i);
			System.out.println(list);
		}
		
		list.remove(2);
		System.out.println(list);
	}
}