/*
	Another topic is the Iterator class. Like Comparable, Iterator is a 
	method to allow a class to be used more easily by other classes or the 
	standard library.
	Methods:
		Required: hasNext(), next()
		Optional: forEachRemaining(Consumer<? super E>), remove()
	
	Note that optional methods will throw UnsupportedOperationException if 
	they were not defined in the implementing class.
*/

import java.util.Iterator;
import java.util.NoSuchElementException;

public class StringArrayIterator implements Iterator<String> {
	private String[] array;
	private int index = 0,
				count = 0;
	
	public StringArrayIterator(String[] array) {
		this(array, array.length);
	}
	
	public StringArrayIterator(String[] array, int count) {
		this.array = array;
		this.count = count;
	}
	
	public boolean hasNext() {
		return index < count; // Because .length = (max index + 1)
	}
	
	public String next() {
		if (!hasNext())
			throw new NoSuchElementException();
		return array[index++];
	}
	
	public String[] iterator() {
		return array;
	}
	
	public static void main(String[] args) {
		String[] array = {"A", "B", "C", "D", "E"};
		StringArrayIterator it = new StringArrayIterator(array);
		
		while (it.hasNext())
			System.out.println(it.next());
		
		for (String item : it.iterator())
			System.out.println(item);
	}
}