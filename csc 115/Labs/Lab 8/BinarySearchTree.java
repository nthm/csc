/*
	E in this class must extend Comparable or else it will not compile. This 
	is because we'll use the method .compareTo() from the Comparable class.
	
	Idea:
	Child extends Parent
	
	Idea with generics:
	Child<Species> extends Parent<Species>
	
	Idea with generics that extend:
	Child<Species extends Subset<wildcard super E>> extends Parent<Species>
*/

public class BinarySearchTree<E extends Comparable<? super E>> extends BinaryTree<E> {
	
	public void add(E obj) {
		root = add(root, obj);
	}
	
	// Recursive therefore private
	private TreeNode<E> add(TreeNode<E> node, E obj) {
		// Base case if node is null
		if (node == null) return new TreeNode<E>(obj);
		
		int compare = obj.compareTo(node.item);
		if (compare < 0)
			node.left  = add(node.left , obj);
		else if (compare > 0)
			node.right = add(node.right, obj);
		else
			throw new RuntimeException("Duplicates are not allowed in the tree.")
		return node
	}
	
	public static main(String[] args) {
		BinarySearchTree<String> bst = new BinarySearchTree<>();
		bst.add("Z");
		bst.add("B");
		bst.add("F");
		DrawableBTree<String> treeA = new DrawableBTree<>(bst);
		treeA.showFrame();
	}
}