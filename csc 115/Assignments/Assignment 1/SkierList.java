/*
	Name:	Grant Hames-Morgan
	ID:		V00857826
	Date:	Frday, January 8th, 2016
	Filename:	SkierList.java
	Details: CSC115 Assignment 1
*/

/**
	Class SkierList holds a list of Skiers (see Skier.java) in no particular 
	order.
*/

public class SkierList {
	private Skier[] skiers;
	private int count;
	private static final int INITIAL_CAP = 3;

	/**	Creates a new list of skiers of the default capacity.
	*/
	public SkierList() {
		this.skiers = new Skier[INITIAL_CAP];
		this.count = 0;
	}

	/**	Retrieves the number of skiers in the list.
		
		@return			Skier count
	*/
	public int size() {
		return this.count;
	}

	/**	Retrieves a skier at the given index. If the index is out of range then 
		{@code null} is returned.
		
		@param index	Position in the list to retrieve
		@return			Skier at index
	*/
	public Skier get(int index) {
		if (index < 0 || index > this.count) return null;
		Skier item = this.skiers[index];
		return item;
	}

	/**	Remove a skier at the given index. If the index is out of range then 
		nothing is removed.
	
		@param index	Position in the list to remove
	*/
	public void remove(int index) {
		// Make sure the index is not out of bounds.
		if (index < 0 || index > this.count) return;
		
		// Don't move all items down if it's the last skier.
		if (index == this.count) {
			this.skiers[index] = null;
		} else {
		/*
			Takes all skiers _after_ the index until count and copy them onto 
			the same array starting _at_ index. This overwrites the skier at 
			the index to be removed.
		*/	
			System.arraycopy(this.skiers, index + 1, // Index to start copying
							 this.skiers, index,	 // Index to start pasting
							 count - (index + 1));	 // How many items to move
		}
		
		this.count--;
	}
	
	/**	Adds a skier to the end of the list. If the skier is already in the 
		list then nothing is added.
		
		@param skier	Skier to be added
	*/
	public void add(Skier skier) {
		// Make sure the skier is not already in the list
		if (findSkier(skier) != -1) return;
		
		// Double the array size if it's full
		if (this.count == this.skiers.length) {
			Skier[] tmp = new Skier[2 * this.count];
			System.arraycopy(this.skiers, 0, tmp, 0, count);
			
			this.skiers = tmp;
		}
		
		this.skiers[count] = skier;
		this.count++;
	}

	/**	Looks for a given skier and retrieves its index if found. Returns -1 if 
		the skier could not be found.
	
		@param skier	Skier to look for
		@return			Index of the skier if found. -1 otherwise.
	*/
	public int findSkier(Skier skier) {
		for (int i = 0; i < this.count; i ++) {
			if (this.skiers[i].equals(skier)) return i;
		}
		
		return -1; // Not in the list
	}

	/**
	 * Used primarily as a test harness for the class.
	 * @param args Not used.
 	 */
	public static void main(String[] args) {
		System.out.println("Testing the SkierList class.");
		SkierList list = null;
		try {
			list = new SkierList();
		} catch (Exception e) {
			System.out.println("Constructor not working.");
			e.printStackTrace();
			return;
		}
		// Add some skiers.
		Skier s1 = new Skier("Daffy Duck", 0);
		list.add(s1);
		if (list.size() != 1) {
			System.out.println("Failed at test one.");
			return;
		}
		if (!list.get(0).equals(s1)) {
			System.out.println("Failed at test two.");
			System.out.println("The first skier in the list needs to be in index position 0");
			return;
		}
		if (list.findSkier(s1) == -1) {
			System.out.println("Failed at test three.");
			return;
		}
		Skier s2 = new Skier("Bugs Bunny", 4);
		list.add(s2);
		if (s2.getLevel() != 4) {
			System.out.println("Failed at test four.");
			return;
		}
		if (list.size() != 2) {
			System.out.println("Failed at test five.");
			return;
		}
		Skier tmp1 = list.get(0);
		Skier remaining;
		if (tmp1.equals(s1)) {
			remaining = s2;
		} else {
			remaining = s1;
		}
		list.remove(0);
		if (list.findSkier(remaining) == -1) {
			System.out.println("Failed at test six.");
			return;
		}
		if (list.size() != 1) {
			System.out.println("Failed at test seven.");
			return;
		}
		if (!list.get(0).equals(remaining)) {
			System.out.println("Failed at test eight.");
			return;
		}
		list.remove(0);
		if (list.size() != 0) {
			System.out.println("Failed at test nine.");
			return;
		}
		System.out.println("Testing for expansion.");
		// note that the array has to expand in this test.
		// Creating an initial array with a length >= max is a failure.
		String prefix = "Skier";
		int max = 1000;
		try {
			for (int i=0; i<max; i++) {
				list.add(new Skier(prefix+i));
			}
		} catch (Exception e) {
			System.out.println("Failed at test 10.");
			e.printStackTrace();
			return;
		}
		
		for (int i=max-1; i>=0; i--) {
			if (list.findSkier(new Skier(prefix+i)) == -1) {
				System.out.println("Failed at test 11.");
				System.out.println("Unable to find skier: "+(prefix+i));
				return;
			}
		}
		System.out.println("All tests passed");
	}
}
