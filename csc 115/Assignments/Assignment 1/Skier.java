/*
	Name:	Grant Hames-Morgan
	ID:		V00857826
	Date:	Frday, January 8th, 2016
	Filename:	Skier.java
	Details: CSC115 Assignment 1
*/

/**
	Class Skier holds a name and level field in each created object. The name 
	is used as a unique identifier, therefore we can assume no two skiers have 
	the same name. The possible levels and their descriptions are as follows: 
	
	<ol start="0">
		<li>Never skied before.
		<li>Cautious novice: able to snow-plow, stop and make round turns on easy trails.
		<li>Cautious, yet confident skiers who can link turns at moderate speeds.
		<li>Confident skiers who can ski mostly parallel but may use wedging to turn or stop on steep or icy trails.
		<li>Confident skiers who can make parallel turns but do not ski advanced trails.
	</ol>
*/

public class Skier {
	private String name;
	private int level;
	
	/**	Creates a new skier level 0 skier.
	
		@param name		The unique name of the skier
	*/
	public Skier(String name) {
		this.name  = name;
		this.level = 0;
	}

	/**	Creates a new skier of the given level. If the level is 
		less than 0 or greater than 4 it defaults to 0.
		
		@param name		The unique name of the skier
		@param level 	Skier's level. Must be between 0 and 4 (inclusive)
	*/
	public Skier(String name, int level) {
		this.name  = name;
		if (level > 4 || level < 0) level = 0;
		this.level = level;
	}

	/**	Updates the name of the skier.
	 	
		@param name		The unique name of the skier
	*/
	public void setName(String name) {
		this.name = name;
	}
	
	/** Retrieves the name of the skier.
	
		@return			Skier's name
	*/
	public String getName() {
		return this.name;
	}

	/**	Updates the skier's level. Follows the same level restriction as the 
		Skier constructor.
		
		@param level	Skier's level. Must be between 0 and 4 (inclusive)
	*/
	public void setLevel(int level) {
		if (level > 4 || level < 0) return; // Don't change the level
		this.level = level;
	}

	/**	Retrieves the level of the skier.
		
		@return			Skier's level
	*/
	public int getLevel() {
		return this.level;
	}
	
	/**	Determines if the given skier object is equivalent to the skier. In 
		this implementation they are equalif they have the same name.
		
		@param other	The skier object to compare
		@return			True if skier objects are equivalent, false otherwise
	*/
	public boolean equals(Skier other) {
		if (this.name.equalsIgnoreCase(other.name)) return true;
		return false;
	}

	/**	Overrides the toString() method from java.lang.Object and returns 
		both the name and level of the skier (syntax below).
		
		@return			String of the format {@code [name] (level [level])}
						Ex: Tyler (level 2)
	*/
	@Override
	public String toString() {
		return this.name + " (level " + this.level + ")";
	}

	public static void main(String[] args) {
		System.out.println("Testing the Skier class.");
		Skier s1 = null;
		try {
			s1 = new Skier("Howie SnowSkier", 4);
		} catch(Exception e) {
			System.out.println("Constructor not working.");
			e.printStackTrace();
			return;
		}
		if (!s1.getName().equals("Howie SnowSkier")) {
			System.out.println("Failed at test one.");
			return;
		}
		if (s1.getLevel() != 4) {
			System.out.println("Failed at test two.");
			return;
		}
		Skier s2 = new Skier("Baby Skier");
		if (!s2.getName().equals("Baby Skier")) {
			System.out.println("Failed at test three.");
			return;
		}
		if (s2.getLevel() != 0) {
			System.out.println("Failed at test four.");
			return;
		}
		s2.setName("Better Skier");
		s2.setLevel(1);
		if (!s2.getName().equals("Better Skier") || s2.getLevel() != 1) {
			System.out.println("Failed at test five.");
			return;
		}
		if (s1.equals(s2)) {
			System.out.println("Failed at test six.");
			return;
		}
		if (!s1.equals(new Skier("Howie SnowSkier",4))) {
			System.out.println("Failed at test seven.");
			return;
		}
		if (s1.toString().equals("Howie SnowSkier (level 0)")) {
			System.out.println("Failed at test eight.");
			System.out.println("Expected: Howie SnowSkier (level 0)");
			System.out.println("Got:      "+s1.toString());
			return;
		}
		System.out.println("All tests passed.");
	}
}
	
