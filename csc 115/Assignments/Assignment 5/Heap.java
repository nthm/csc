import java.util.NoSuchElementException;
import java.util.Random;

/*
	Name:     Grant Hames-Morgan
	ID:       V00857826
	Date:     Sunday, April 3rd, 2016
	Filename: Heap.java
	Details:  CSC115 Assignment 5: Emergency Room
*/

@SuppressWarnings({"unchecked"})

public class Heap {

	private Comparable[] heapArray;
	private int size;
	private static final int INITIAL_CAP = 10;

	/**	Creates a new empty heap
	*/
	public Heap() {
		heapArray = new Comparable[INITIAL_CAP];
		size = 0;
	}
	
	private boolean lessOrEqual(int a, int b) {
		return heapArray[a].compareTo(heapArray[b]) <= 0;
	}
	
	/**	Helper to swap two nodes by their given indexes
	*/
	private void swap(int a, int b) {
		Comparable x = heapArray[a];
		heapArray[a] = heapArray[b];
		heapArray[b] = x;
	}
	
	/**	Restore the number of spaces (padding) at the end of the heap array 
		back to INITIAL_CAP
	*/
	private void adjustHeapArray() {
		Comparable[] newHeap = new Comparable[size + INITIAL_CAP];
		System.arraycopy(heapArray, 1, newHeap, 1, size); // Transfer items
		heapArray = newHeap;
	}
	
	/**	Inserts an item into the heap
	
		@param item		The item to insert
	*/
	public void insert(Comparable item) {
		if (heapArray.length - size == 1) adjustHeapArray();
		heapArray[++size] = item;
		
		// Raise the element into the heap
		for (int i = size; i > 1 && !lessOrEqual(i / 2, i); i /= 2)
			swap(i, i / 2);
	}
	
	/**	Retrieves the root item of the heap (without removing it)
	
		@returns		The root item
		@throws			NoSuchElementException if the heap is empty
	*/
	public Comparable getRootItem() {
		if (isEmpty()) throw new NoSuchElementException();
		return heapArray[1]; // [0] is unused
	}
	
	/**	Retrieves the root item of the heap and removes it
	
		@returns		The root item
		@throws			NoSuchElementException if the heap is empty
	*/
	public Comparable removeRootItem() {
		// Save root - throwing exception if needed
		Comparable rootItem = getRootItem();
		
		heapArray[1] = heapArray[size];
		heapArray[size] = null;
		size--;
		
		// Lower the element into the heap
		int root = 1;
		do {
			// Pick smallest child
			int child = 2 * root;
			if (child >= size) break;
			if (lessOrEqual(child + 1, child)) child++;
			
			// If parent is smaller than smallest child, exit. Else, swap
			if (lessOrEqual(root, child)) break;
			swap(root, child);
			
			// Test the next node and its children
			root = child;
		} while (2 * root < size);
		
		// Shrink array if needed (in this case 20 spaces)
		if (heapArray.length - size > 2 * INITIAL_CAP) adjustHeapArray();
		return rootItem;
	}
	
	/**	@returns		The size of the heap
	*/
	public int size() {
		return size;
	}
	
	/**	@returns		True if the heap is empty, false otherwise
	*/
	public boolean isEmpty() {
		return size == 0;
	}
	
	@Override
	public String toString() {
		String list = "";
		for (int index = 1; index < size; index++)
			list += heapArray[index] + " ";
		return list;
	}
	
	/**	Internal testing
	
		@param args		Not used
	*/
	public static void main(String[] args) {
		Heap heap = new Heap();
		Random rand = new Random();
		
		for (int i = 1; i < 20; i++) {
			heap.insert(rand.nextInt(20));
			System.out.println(heap.toString());
		}
		System.out.println("Size: " + heap.size());
		System.out.println("Empty? (should be false) " + heap.isEmpty());
		
		for (int i = 1; i < 20; i++) {
			System.out.println("Removed: " + heap.removeRootItem());
			System.out.println(heap.toString());
		}
		System.out.println("Empty? (should be true) " + heap.isEmpty());
	}
}
