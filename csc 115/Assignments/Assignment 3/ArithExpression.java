/*
	Name:     Grant Hames-Morgan
	ID:       V00857826
	Date:     Tuesday, Feb 28th, 2016
	Filename: ArithExpression.java
	Details:  CSC115 Assignment 3: Calculator
*/

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;

/*
	The ArithExpression is an arithmetic expression that has a single real 
	number solution. Allowable arithmetic operators are ^, *, /, +, and -. All 
	real numbers are allowed, except in the case of the exponent in the power 
	'^' operation, which must only be a non-negative integer. Allowable 
	parentheses are ( and ) only.
*/

public class ArithExpression {

	private TokenList postfixTokens;
	private TokenList infixTokens;

	/**
	 * Sets up a legal standard Arithmetic expression.
	 * The only parentheses accepted are "(" and ")".
	 * @param word An arithmetic expression in standard infix order.
	 * 		An invalid expression is not expressly checked for, but will not be
	 * 		successfully evaluated, when the <b>evaluate</b> method is called.
	 * @throws InvalidExpressionException if the expression cannot be properly parsed,
	 *  	or converted to a postfix expression.
	 */
	public ArithExpression(String word) {
		if (Tools.isBalancedBy("()", word)) {
			tokenizeInfix(word);
			infixToPostfix();
		} else {
			throw new InvalidExpressionException("Parentheses unbalanced");
		}
	}

	/*
	 * A private helper method that tokenizes a string by separating out
	 * any arithmetic operators or parens from the rest of the string.
	 * It does no error checking.
	 * The method makes use of Java Pattern matching and Regular expressions to
	 * isolate the operators and parentheses.
	 * The operands are assumed to be the substrings delimited by the operators and parentheses.
	 * The result is captured in the infixToken list, where each token is 
	 * an operator, a paren or a operand.
	 * @param express The string that is assumed to be an arithmetic expression.
	 */
	private void tokenizeInfix(String express) {
		infixTokens = new TokenList(express.length());

		// regular expression that looks for any operators or parentheses.
		Pattern opParenPattern = Pattern.compile("[-+*/^()]");
		Matcher opMatcher = opParenPattern.matcher(express);

		String matchedBit, nonMatchedBit;
		int lastNonMatchIndex = 0;
		String lastMatch = "";

		// find all occurrences of a matched substring
		while (opMatcher.find()) {
			matchedBit = opMatcher.group();
			// get the substring between matches
			nonMatchedBit = express.substring(lastNonMatchIndex, opMatcher.start());
			nonMatchedBit = nonMatchedBit.trim(); //removes outside whitespace
			// The very first '-' or a '-' that follows another operator is considered a negative sign
			if (matchedBit.charAt(0) == '-') {
				if (opMatcher.start() == 0 || 	
					!lastMatch.equals(")") && nonMatchedBit.equals("")) {
					continue;  // ignore this match
				}
			}
			// nonMatchedBit can be empty when an operator follows a ')'
			if (nonMatchedBit.length() != 0) {
				infixTokens.append(nonMatchedBit);
			}
			lastNonMatchIndex = opMatcher.end();
			infixTokens.append(matchedBit);
			lastMatch = matchedBit;
		}
		// parse the final substring after the last operator or paren:
		if (lastNonMatchIndex < express.length()) {
			nonMatchedBit = express.substring(lastNonMatchIndex,express.length());
			nonMatchedBit = nonMatchedBit.trim();
			infixTokens.append(nonMatchedBit);
		}
	}

	/**
	 * Determines whether a single character string is an operator.
	 * The allowable operators are {+,-,*,/,^}.
	 * @param op The string in question.
	 * @return True if it is recognized as a an operator.
	 */
	public static boolean isOperator(String op) {
		switch(op) {
			case "+": case "-": case "/": case "*": case "^":
				return true;
			default:
				return false;
		}
	}
/*
	Algorithm planning:
	Ended up using another algorithm after issues with this one.

	Infix: 2 + (3 ^ 5 * 7) / 8
	Convert to Postfix:
	  Scan the whole thing for operand of greatest precedence skipping items in ()
		Found: / so take the two items on either side of it: ( ... ) / 8
		  Redo a) with 3 ^ 5 * 7
			Found ^, 3 ^ 5, take either side and flip: 3 5 ^
			  Save it as A.
			Found *, A * 7, take either side and flip: A 7 *
			  Save it as B.
		  Done so return it back up
		We now have B / 8 now so flip: 8 B / save it as C
	  Rescan to find a new operand to work with
		Found +, 2 + C, take either side and flip: 2 C +

	Expand all:
	2 C +
	2 8 B / +
	2 8 7 A * / +
	2 8 7 3 5 ^ * / +

	Evaluation:
	2 8 7 3 5 read ^ pop 2, eval: 3 ^ 5 = 243
	2 8 7 243 read * pop 2, eval: 243 * 7 = 1701
	2 8 1701  read / pop 2, eval: 1701 / 8 = 212.625
	2 212.625 read + pop 2, eval: 212.625 + 2 = 214.625
*/
	/**	
		A private method to convert the tokenized infix expression (handled by 
		tokenizeInfix, another private method) into a tokenized postfix 
		expression.
		
		@throws InvalidExpressionException if the parentheses are mismatched.
	*/
	private void infixToPostfix() {
		int size = infixTokens.size();
		postfixTokens = new TokenList(size);
		StringStack operators = new StringStack();
		
		Map<String, Integer> precedence = new HashMap<String, Integer>() {{
			put("^", 4); put("/", 3); put("*", 3); put("+", 2); put("-", 2);
		}};
	/*
		Operator associativity will not be handled in the same way as 
		precedence because '^' is the only operator to have a different value.
		
		It will be hardcoded to save time and memory.
	*/
		for (int i = 0; i < size; i++) {
			String token = infixTokens.get(i);
			
			if (isOperator(token)) {
				while (!operators.isEmpty()) {
					if (operators.peek().equals("(")) break; // Not in precedence
					
					int precToken = precedence.get(token),
						precStack = precedence.get(operators.peek());
					
					if ((!token.equals("^") && precToken == precStack) || precStack > precToken)
						postfixTokens.append(operators.pop());
					else break;
				}
				operators.push(token); // Push operator
				
			} else if (token.equals("(")) {
				operators.push(token);
				
			} else if (token.equals(")")) {
				while (!operators.peek().equals("("))
					postfixTokens.append(operators.pop());
				operators.pop(); // Remove ")"
				
			} else { // Number
				postfixTokens.append(token);
			}
		/*
			Was used for debugging in main()
			System.out.format("%-10s%-25s%-15s\n", token, postfixTokens.toString(), operators.peek());
		*/
		}
		
		while (!operators.isEmpty()) {
			String operator = operators.pop();
			
			if (operator.matches("[()]"))
				throw new InvalidExpressionException("Mismatched parentheses");
			postfixTokens.append(operator);
		}
	}

	/**	@return A string representing the tokenized infix expression where 
				every token is seperated by a space.
	*/
	public String getInfixExpression() {
		return infixTokens.toString();
	}

	/**	@return A string representing the tokenized postfix expression, 
				after being converted from infix, where every token is 
				seperated by a space.
	*/
	public String getPostfixExpression() {
		return postfixTokens.toString();
	}
	
	/**	Evaluates the expression.
	
		@return		The numerical value of the expression.
		@throws		InvalidExpressionException if the expression causes an 
					arithmatic error such as division by zero.
	*/
	public double evaluate() {
		StringStack number = new StringStack();
		int expSize = postfixTokens.size();
		
		// Assigned later in the try/catch
		double total = 0.0;
		
		try {
			for (int i = 0; i < expSize; i++) {
				String token = postfixTokens.get(i);

				if (!isOperator(token)) {
					number.push(token);
					continue; // Collect everything other than operands
				}

				double numB = Double.parseDouble(number.pop()),
					   numA = Double.parseDouble(number.pop()),
					   result = 0.0;

				switch(token) {
					case "+": result = numA + numB;
						break;
					case "-": result = numA - numB;
						break;
					case "*": result = numA * numB;
						break;
					case "/":
						if (numB == 0)
							throw new InvalidExpressionException("Division by zero");
						result = numA / numB;
						break;
					case "^":
						// Math.pow() allows negative exponents so why is it restricted?
						if (numB <= 0)
							throw new InvalidExpressionException("Exponents must be positive");
						result = Math.pow(numA, numB);
						break;
					// No default becuase data is filtered in isOperator()
				}

				// Double to String: http://stackoverflow.com/questions/5766318/
				number.push(String.valueOf(result));

			}
			
			total = Double.parseDouble(number.pop());
			
		} catch (Exception err) {
			throw new InvalidExpressionException("Arithmetic Exception: " + err);
		}
		
		if (!number.isEmpty())
			throw new InvalidExpressionException("Too few operands");
			
		return total;
	}
/*
	The PDF included with this assignment states: "Both the infxToPostfix and 
	evaluate methods should call smaller private methods when possible."
	
	I haven't split the code of either method into smaller modules as doing so 
	increases code complexity and execution time - there's no reason to do so 
	in this code.
*/
	/**	Used to test the class through a list of test expressions and answers.
	
		@param args		Not used.
	*/
	public static void main(String[] args) {
		// Syntax: { expression, expected answer }
		String[][] tests = {
			{ "2 + 5^7 * 8"            , "625002.0" },
			{ "8 + 4 * 2 / (1 - 5)^2^4", "8.000000001862645" },
			{ "2 + (3^5 * 7) / 6"      , "285.5" }
		};
		
		for (String[] line : tests) {
			String exp = line[0];
			Double correctAnswer = Double.parseDouble(line[1]);
			
			System.out.println("Testing: " + exp);
			ArithExpression toTest = new ArithExpression(exp);
			
			System.out.println(
				  "Tokens:"
				+ "\n   Infix:   " + toTest.getInfixExpression() 
				+ "\n   Postfix: " + toTest.getPostfixExpression()
			);
			
			String status = Double.compare(toTest.evaluate(), correctAnswer)  == 0 ? "Passed" : "Failed";
			System.out.println(status + " .evaluate()\n");
		}
		
		Scanner console = new Scanner(System.in);
		while (true) {
			System.out.print("Expression: ");
			System.out.println(new ArithExpression(console.nextLine()).evaluate());
		}
	}
}
