/*
	Name:     Grant Hames-Morgan
	ID:       V00857826
	Date:     Tuesday, Mar 12th, 2016
	Filename: BinaryTree.java
	Details:  CSC115 Assignment 4: Patient Location
*/

/*
	The shell of the class, to be completed as part of CSC115 Assignment 4: 
	Patient Location.
*/
 
public class BinaryTree<E> {

	/* The root is inherited by any subclass
	 * so it must be protected instead of private.
	 */
	protected TreeNode<E> root;

	/**
	 * Create an empty BinaryTree.
	 */
	public BinaryTree() {}

	/**
	 * Create a BinaryTree with a single item.
	 * @param item The only item in the tree.
	 */
	public BinaryTree(E item) {
		root = new TreeNode<E>(item);
	}

	/**
	 * Used only by subclasses and classes in the same package (directory).
	 * @return The root node of the tree.
	 */
	protected TreeNode<E> getRoot() {
		return root;
	}
	
	/**	Determines the height of the tree (the longest branch from the root 
		node to a leaf node).
		
		@returns		The height
	*/
	public int height() {
		return height(root);
	}
	
	/**	Used by the public method height(). Recursively calls each node in the 
		tree and adds returns 1 unless a leaf is reached.
		
		@param node		The node to inspect
		@returns		1 or 0 with every iteration, eventually the height of 
		        		the tree.
	*/
	private int height(TreeNode<E> node) {
		if (node == null) return 0;
		return 1 + Math.max(height(node.right), height(node.left));
	}

	/**	@return True if the tree is empty
	*/
 	public boolean isEmpty() {
		return root == null;
	}
	
	/** Removes all nodes from the tree
	*/
	public void makeEmpty() {
		root = null;
	}
}