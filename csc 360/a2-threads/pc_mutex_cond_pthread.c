#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <pthread.h>

#define MAX_ITEMS 10
const int NUM_ITERATIONS = 200;
const int NUM_CONSUMERS  = 2;
const int NUM_PRODUCERS  = 2;
const int NUM_THREADS = NUM_CONSUMERS + NUM_PRODUCERS;

int producer_wait_count;     // # of times producer had to wait
int consumer_wait_count;     // # of times consumer had to wait
int histogram [MAX_ITEMS+1]; // histogram [i] == # of times list stored i items

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t add_ok_signal = PTHREAD_COND_INITIALIZER;
pthread_cond_t remove_ok_signal = PTHREAD_COND_INITIALIZER;
int items = 0;

void* producer (void* v) {
  for (int i=0; i<NUM_ITERATIONS; i++) {
    pthread_mutex_lock(&mutex);
    assert(items >= 0 && items <= MAX_ITEMS);
    // use `while` out of fear for "spurious wakeups" of threads
    while (items == MAX_ITEMS) {
      // wait for consumers to signal that it's OK to add to the buffer
      producer_wait_count++;
      pthread_cond_wait(&add_ok_signal, &mutex);
    }
    items++;
    histogram[items]++;
    pthread_cond_signal(&remove_ok_signal);
    pthread_mutex_unlock(&mutex);
  }
  return NULL;
}

void* consumer (void* v) {
  for (int i=0; i<NUM_ITERATIONS; i++) {
    pthread_mutex_lock(&mutex);
    assert(items >= 0 && items <= MAX_ITEMS);
    // use `while` out of fear for "spurious wakeups" of threads
    while (items == 0) {
      // wait for producers to signal that it's OK to remove from the buffer
      consumer_wait_count++;
      pthread_cond_wait(&remove_ok_signal, &mutex);
    }
    items--;
    histogram[items]++;
    pthread_cond_signal(&add_ok_signal);
    pthread_mutex_unlock(&mutex);
  }
  return NULL;
}

int main (int argc, char** argv) {
  int rc[NUM_THREADS];
  pthread_t t[NUM_THREADS];

  // create producer threads
  for (int i = 0; i < NUM_PRODUCERS; i++) {
    rc[i] = pthread_create( &t[i], NULL, &producer, NULL);
    if (rc[i]) {
      printf("thread creation failed: %d\n", rc[i]);
    }
  }

  // create consumer threads (NUM_THREADS - NUM_PRODUCERS) of them
  for (int i = NUM_PRODUCERS; i < NUM_THREADS; i++) {
    rc[i] = pthread_create( &t[i], NULL, &consumer, NULL);
    if (rc[i]) {
      printf("thread creation failed: %d\n", rc[i]);
    }
  }

  // block the main thread by joining them
  for (int i = 0; i < NUM_THREADS; i++) {
    pthread_join(t[i], NULL);
  }

  printf ("producer_wait_count=%d\nconsumer_wait_count=%d\n", producer_wait_count, consumer_wait_count);
  printf ("items value histogram:\n");
  int sum = 0;
  for (int i = 0; i <= MAX_ITEMS; i++) {
    printf ("  items=%d, %d times\n", i, histogram [i]);
    sum += histogram [i];
  }
  assert (sum == sizeof (t) / sizeof (pthread_t) * NUM_ITERATIONS);
  return EXIT_SUCCESS;
}
