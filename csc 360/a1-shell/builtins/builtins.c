#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h> // chdir
#include <time.h>
#include "history_list.h"

int launch(char *line);

// Global history list. There is only one.
HistoryItem* most_recent;

int run_builtins(char **args) {
  char *cmd = args[0];

  if (strcmp(cmd, "setenv") == 0) {
    // TODO: Test for failures by putting a `=` in the name

    // XXX: Could issue a warning or safety fallback if they try to change
    // $HOME or $PATH as that can be damaging to the session. However, not
    // allowed to write to stdout to explain.

    if (args[2] == NULL) {
      args[2] = "";
    }
    // Let the syscall return the status. Note the `1` allows overwriting
    return setenv(args[1], args[2], 1);
  }

  if (strcmp(cmd, "unsetenv") == 0) {
    if (args[1] == NULL) {
      return 1;
    }
    // Let the syscall return the status
    return unsetenv(args[1]);
  }

  if (strcmp(cmd, "cd") == 0) {
    if (args[1] == NULL) {
      args[1] = getenv("HOME");
    }
    if (chdir(args[1]) != 0) {
      perror("Not able to change directory");
    }
    return 0;
  }

  if (strcmp(cmd, "exit") == 0) {
    exit(EXIT_SUCCESS);
    return 0;
  }

  if (strcmp(cmd, "!") == 0) {
    char* prefix = args[1];
    printf("Searching for previous command: %s[...]\n", prefix);
    HistoryItem* cur = history_list->most_recent;
    if (cur->prev == NULL || cur->prev->prev == NULL) {
      return 0;
    }
    // Skip the entry for history itself
    cur = cur->prev;
    for (; cur->prev != NULL; cur = cur->prev) {
      if (strncmp(prefix, cur->command, strlen(prefix)) == 0) {
        printf("Replaying: %s\n", cur->command);
        return launch(cur->command);
      }
    }
    return 0;
  }

  if (strcmp(cmd, "history") == 0) {

    if (args[1] != NULL && strcmp(args[1], "clear") == 0) {
      history_clear();
      printf("History cleared\n");
      return 0;
    }

    HistoryItem* cur = history_list->most_recent;
    if (cur->prev == NULL || cur->prev->prev == NULL) {
      return 0;
    }
    // Skip the entry for history itself
    cur = cur->prev;
    for (; cur->prev != NULL; cur = cur->prev) {
      char* time_str = ctime(&cur->start_time);
      time_str[strlen(time_str)-1] = '\0';
      printf(
        "[%s]+%fs %s (%d)\n",
        time_str, cur->duration, cur->command, cur->return_status);
    }
    // Again, skip one entry
    printf("%d items\n", history_list->length - 1);

    return 0;
  }

  return -1;
}
