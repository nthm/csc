# Kapish

Building:

```sh
> make
gcc -pedantic-errors -Wall -std=c11 kapish.c builtins/builtins.c builtins/history_list.c -o kapish
> ./kapish
```

This shell implements the extra history features via `history`, `history clear`
and `!commandprefix`.

There are no known bugs.
