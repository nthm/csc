#ifndef LL
#define LL

typedef struct Node Node;
struct Node {
    unsigned char hex;
    Node* prev;
    Node* next;
};

typedef struct List List;
struct List {
    Node* head;
    Node* end;
    Node* cursor;
    unsigned int length;
};

List* ll                ();
Node* ll_node           (unsigned char hex);
void  ll_prev           (List* leader);
void  ll_next           (List* leader);
void  ll_append         (List* leader, Node* to_add);
void  ll_prepend        (List* leader, Node* to_add);
void  ll_cursor_prepend (List* leader, Node* to_add);
void  ll_cursor_append  (List* leader, Node* to_add);
void  ll_print          (List* leader);
Node* ll_unlink         (List* leader);
void  ll_empty          (List* leader);

#endif
