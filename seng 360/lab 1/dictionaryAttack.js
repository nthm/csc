const crypto = require('crypto')
const fs = require('fs')

const plaintext = 'This is a top secret.'
const iv = new Buffer.from('aabbccddeeff00998877665544332211', 'hex')
const ciphertext = '764aa26b55a4da654df6b19e4bce00f4ed05e09346fb0e762583cb7da2ac93a2'

// Believe it or not this is not in a Buffer.from()
// const ciphertext = '0c1a6d1f180a3173244ef96a08684e7912832c8e56ca214f1fdf6e3c192af75d'

const wordDictionary =
  fs.readFileSync('./360 Lab 1 English words.txt', 'utf8')
    .split(/\r?\n/)

let tryCipher = undefined
let tryCiphertext = ''

for (let word of wordDictionary) {
  if (word.length > 16) {
    console.log('Word longer than 16 characters, skipping')
    continue
  }
  while (word.length < 16) {
    word += '#'
  }
  tryCipher = crypto.createCipheriv('aes-128-cbc', word, iv)
  tryCiphertext = tryCipher.update(plaintext, 'utf8', 'hex') + tryCipher.final('hex')

  console.log(word) //, (new Buffer.from(word)).toString('hex'), tryCiphertext, ciphertext)
  if (tryCiphertext === ciphertext) {
    console.log('Found password:', word)
    process.exit(0)
  }
}

console.log(`Couldn't find the password`)
