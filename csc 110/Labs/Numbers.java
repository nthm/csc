import java.util.Scanner;
import java.io.*;

public class Numbers {
	public static void main(String[] args) {
		double sum = 0.0;
		int lines = 1;
		try {
			Scanner reader = new Scanner(new File("Numbers.txt"));
			while (reader.hasNextDouble()) {
				double number = reader.nextDouble();
				System.out.println("Current: " + "'" + number + "'");
				sum += number;
				lines++;
			}
			
			double average = sum / lines;
			System.out.println("Average: " + average);

		} catch (Exception err) {
			System.out.println("Didn't work.\n" + err);
		}
	}
}
