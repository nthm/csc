/*
 * ID: 	V00857826 Grant Hames-Morgan
 * FirstAssignment
 * Prints ASCII art and then the surface area of a cylinder.
 * Program takes no input and outputs to stdout.
 */

import java.util.*;

public class FirstAssignment {

	public static void main(String[] args) {
		System.out.println("Welcome!\n");

		printTotemPole();
		calcSurfaceArea();
	}

	public static String[] printPig() {
		String[] drawing = {"^--^", "(0  0)", "(oo)", "(\")_(\")@"};
		return drawing;
	}

	public static String[] printFrog() {
		String[] drawing = {"@..@", "(----)", "( >__< )", "\"\"    \"\""};
		return drawing;
	}

	public static void printTotemPole() {
		int repitition = 2; // Repeat the stack of animals

		Map<String, String[]> animals = new HashMap<String, String[]>();

		animals.put("Pig",	printPig());
		animals.put("Frog",	printFrog());

		String seperator = "/~~~~~~\\";

		for (int i = 0; i < repitition; i++) {
			for (String[] lines : animals.values()) {
				System.out.println(seperator);

				// Calculate padding
				int largestString = 0;
				for (String line : lines) {
					int length = line.length();
					largestString = length > largestString ? length : largestString;
				}

				// Print animal with padding
				for (String line : lines) {
					int length = (largestString - line.length()) / 2; // Will floor

					//http://stackoverflow.com/questions/1235179/
					String padding = new String(new char[length]).replace("\0", " ");
					System.out.println(padding + line);
				}
			}
		}

		// Totem base
		System.out.println(seperator);
		System.out.println(seperator + "\n");
	}

	public static void calcSurfaceArea() {
		int height 	 = 5,
			diameter = 4;

		double radius = diameter / 2,
			   circumference = 2 * Math.PI * radius;

		double rectArea = height * circumference,
			   circleArea = Math.PI * Math.pow(radius, 2),
			   totalSurfaceArea = (2 * circleArea) + rectArea;

		System.out.println("Total surface area is: " + totalSurfaceArea);
	}
}
