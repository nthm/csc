// The real program, in Java, will read a file of two lines for input. However,
// this is simplified to just the list of numbers. Return the largest pair of
// numbers that has the smallest distance between them.

// Distance matters more; the size of the number only acts as a tie breaker

function result(line) {
  const input = line.split(' ').map(Number)
  let [one, two] = input
  // TODO: Avoid abs() in Java version using x > y ? x - y : y - x
  let smallestDistance = Math.abs(one - two)

  for (let i = 0; i < input.length; i++) {
    const x = input[i]
    // Save time. If we're at 0 distance that's the best case; one === two
    // and the only way that will change is if x is larger
    if (smallestDistance === 0 && x <= one) {
      console.log(`Skipping ${x} since ${x} <= ${one}`)
      continue
    }
    for (let j = i + 1; j < input.length; j++) {
      const y = input[j]
      const distance = Math.abs(x - y)
      console.log(`${x} to ${y}: ${distance} vs ${smallestDistance}`)

      if (distance > smallestDistance) {
        continue
      }
      // Don't replace our pair if it's smaller
      if (distance === smallestDistance && x <= one) {
        continue
      }
      console.log('New best match!')
      smallestDistance = distance;
      [one, two] = [x, y];

      // There's no way that another y will be a better fit for x. Leave.
      if (smallestDistance === 0) {
        console.log('Match is ideal for this loop. Skipping the rest')
        break
      }
    }
  }
  return one < two ? `${one} ${two}` : `${two} ${one}`
}

result('1 2 14 18 7 2 50 48 3 38 9 1 50 10 10 17 2')

// TODO: once you get to smallestDistance being 0, just search in the first
// loop for a pair of largest numbers?
