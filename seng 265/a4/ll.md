# Linked list layout

Looking at this a year later (for another class), it's not immediately clear how
this linked list is structured. It's not a doubly linked list, but it's a bit
more than a single one, too.

Here's a diagram:

```
 +--------------------------+ Cursor
 |                          |
 | +---------------+ Prev   |
 | |               |        |
XXXXX   +---+    +-v-+    +-v-+    +---+    +---+
X   X-+->   +---->   +---->   +---->   +---->   |
XXXXX | +---+    +---+    +---+    +---+    +---+
  |   |
  |   | Head
  |
  | Length
```

The X box is the "leader". It exists even when the list is empty. This provides
a few benefits:

- You can see the size of the list via `ll->length` without counting all items.
- There a reference to the previous cursor via `ll->prev`. This gives some of
  the functionality of a doubly linked list without a `prev` per node.
- There's no special cursor data type. A cursor is just a regular node.
