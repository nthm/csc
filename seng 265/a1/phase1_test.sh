#!/usr/bin/fish
for i in (seq -w 1 10)
    ./phase1 -f -i tests/t$i.txt | diff - tests/t$i.ph1
    #./phase1 -b -i tests/t$i.ph1 | diff - tests/t$i.txt
    echo -n test $i': ' 
    if test $status -gt 0
        echo failed
        exit 1
    else
        echo passed
    end
    sleep 1
end
