// Multinomial Naive Bayes text classifier

const terms = new Set<string>();
const categories: {
  [category: string]: {
    documents: number,
    terms: number,
    termFrequency: {
      [term: string]: number
    }
  }
} = {};

function tokenize(text: string) {
  // Anything that's NOT alphanumeric should be removed
  const better =
    text
      .replace(/[^(a-zA-Z0-9)+\s]/g, ' ')
      .split(/\s+/)
      .filter(x => x.length > 1);
  return better;
}

function tokenFrequencyMap(tokens: string[]) {
  return tokens.reduce((table, token) => {
    token in table
      ? table[token]++
      : table[token] = 1;
    return table;
  }, {} as { [token: string]: number });
}

function train(category: string, text: string) {
  if (!categories[category]) {
    categories[category] = {
      documents: 0,
      terms: 0,
      termFrequency: {},
    };
  }
  // Objects are passed by reference so this can be updated directly
  const cat = categories[category];
  cat.documents++;

  const tokens = tokenize(text);
  const frequenciesPerToken = tokenFrequencyMap(tokens);

  for (const token in frequenciesPerToken) {
    terms.add(token);
    const frequency = frequenciesPerToken[token];
    cat.terms += frequency;
    token in cat.termFrequency
      ? cat.termFrequency[token] += frequency
      : cat.termFrequency[token] = frequency;
  }
}

function test(text: string) {
  let maxProbability = -Infinity;
  let chosenCategory = undefined;

  const tokens = tokenize(text);
  const frequenciesPerToken = tokenFrequencyMap(tokens);

  const allDocuments =
    Object.values(categories).reduce((sum, obj) => sum + obj.documents, 0);

  // Which category has max probability for this text
  for (const category in categories) {
    const cat = categories[category];
    // Overall probability vs training documents
    let categoryProbability = Math.log(cat.documents / allDocuments);
    // P(w|c) for each word `w` in the text
    for (const token in frequenciesPerToken) {
      // Term probability
      const wordFrequency = cat.termFrequency[token] || 0;
      const wordCount = cat.terms;
      // Laplace +1
      const tokenProbability = (wordFrequency + 1) / (wordCount + terms.size);
      // P(w|c) for token
      categoryProbability += frequenciesPerToken[token] * Math.log(tokenProbability);
    }
    if (categoryProbability > maxProbability) {
      maxProbability = categoryProbability;
      chosenCategory = category;
    }
  }
  return chosenCategory;
}

export { test, train, terms, categories };
