import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.UF;

public class UFAnimation {
		public static void main (String[] args) {
		StdOut.println("Enter a number: ");
		int N = StdIn.readInt();
		StdDraw.setScale(-1, N);
		StdDraw.clear(StdDraw.BLACK);
		StdDraw.setPenColor(StdDraw.WHITE);

		Point[] arrayPoints = new Point[N * N];
		for (int a = 0; a < N; a++) {
			for (int b = 0; b < N; b++) {
				arrayPoints[a + b] = new Point(a, b);
				StdDraw.filledCircle(a, b, 0.05);
			}
		}

		UF unionFind = new UF(N*N);
		int numberOfEdges = 0;
		while (numberOfEdges < N*N -1) {
			int p = StdRandom.uniform(0, N*N);
			int q = StdRandom.uniform(0, N*N);
			if (unionFind.find(p) != unionFind.find(q)) {
				unionFind.union(p, q);
				StdDraw.line(p / N, p % N, q / N, q % N);
				StdDraw.pause(300);
				numberOfEdges++;
			}
		}
	}   	
}

class Point {
	public int x, y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
