## Hypothesis on percentage of red nodes

Red black trees (along with 2-3 trees) build bottom up. This means when nodes
are added they attach on the bottom of the tree. This may cause a node to have 3
children - only then at this low depth does the node have a red link. As more
are added, a 3 node becomes a 4 node and must split to have no red nodes.

Of the 4 states a node can be in, only 1 state gives it a red colour. That state
is not stable, in the sense that as more nodes are added, it will "_want_" to
eventually become 4, split, and stay as a black node for as long as nodes are
added (i.e forever, in this program that has no node removal methods).

I hypothesize that a red black tree will have far fewer red than black nodes as
nodes are added.

## Result

```
> java RedBlackBST test10.txt
Reading input values from test10.txt
Percent of Red Nodes: 20

> java RedBlackBST test100.txt
Reading input values from test100.txt
Percent of Red Nodes: 3

> java RedBlackBST test1000.txt
Reading input values from test1000.txt
Percent of Red Nodes: 1

> java RedBlackBST
No filename given. Generating 1000 random numbers
Percent of Red Nodes: 1
```

## About the program

This red black tree includes a `percentRed()` method which runs as _O(1)_ once
the tree is constructed. I replaced all instances of `.color = ...` code with
a method, `.setColor(...)`, which tracks the count of red nodes is a variable
stored as part of the tree (_not_ part of each node).

Originally I thought I'd have children nodes tell their parent when a colour
changed, so I made the tree doubly-linked. I didn't go through with the idea,
but left the `.parent` code in the tree.
